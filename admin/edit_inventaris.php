      <?php
include "../koneksi.php";
$id_inventaris=$_GET['id_inventaris'];

$select=mysqli_query($koneksi,"select * from inventaris where id_inventaris='$id_inventaris'");
$data=mysqli_fetch_array($select);
?>

<?php
include "header.php";
?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Edit Inventaris</h3>
						</div>
					</div>
				</div>
			</header>

			<div class="box-typical box-typical-padding">
				

				<h5 class="m-t-lg with-border">Edit Inventaris</h5>

				<form action="update_inventaris.php" method="post">
				<input type="hidden" name="id_inventaris" value="<?php echo $data['id_inventaris'];?>">
				<input type="hidden" name="id_jenis" value="<?php echo $data['id_jenis'];?>">
				<input type="hidden" name="id_ruang" value="<?php echo $data['id_ruang'];?>">
				<input type="hidden" name="id_petugas" value="<?php echo $data['id_petugas'];?>">
				<input type="hidden" name="kode_inventaris" value="<?php echo $data['kode_inventaris'];?>">
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Nama</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="nama" required type="text" class="form-control" id="inputPassword" value="<?php echo $data['nama'];?>"></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Kondisi</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="kondisi" required type="text" class="form-control" id="inputPassword" value="<?php echo $data['kondisi'];?>"></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Spesifikasi</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="spesifikasi" required type="text" class="form-control" id="inputPassword" value="<?php echo $data['spesifikasi'];?>"></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Keterangan</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="keterangan" required type="text" class="form-control" id="inputPassword" value="<?php echo $data['keterangan'];?>"></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Jumlah</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="jumlah" required type="text" class="form-control" id="inputPassword" value="<?php echo $data['jumlah'];?>"></p>
						</div>
					</div>
					<div class="form-group row">
						<label for="exampleSelect" class="col-sm-2 form-control-label">Id Jenis</label>
						<div class="col-sm-10">
							<input id="exampleSelect" class="form-control" name="id_jenis" required disabled="" value="<?php echo $data['id_jenis'];?>">	
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Tanggal Register</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input name="tgl_register" required type="date" class="form-control" id="inputPassword" value="<?php echo $data['tgl_register'];?>" readonly></p>
						</div>
					</div>
					<div class="form-group row">
						<label for="exampleSelect" class="col-sm-2 form-control-label">Id Ruang</label>
						<div class="col-sm-10">
							<input id="exampleSelect" class="form-control" name="id_ruang" required disabled="" value="<?php echo $data['id_ruang'];?>">	
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Kode Inventaris</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input name="kode_inventaris" required disabled="" type="text" class="form-control" id="inputPassword" value="<?php echo $data['kode_inventaris'];?>"></p>
						</div>
					</div>
					<div class="form-group row">
						<label for="exampleSelect" class="col-sm-2 form-control-label">Id Petugas</label>
						<div class="col-sm-10">
							<input id="exampleSelect" class="form-control" name="id_petugas" required disabled="" value="<?php echo $data['id_petugas'];?>">	
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Sumber</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="sumber" required type="text" class="form-control" id="inputPassword" value="<?php echo $data['sumber'];?>"></p>
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-inline btn-primary">Simpan</button>
						<a href="inventaris.php" type="button" class="btn btn-inline btn-secondary">Cancel</a>
					</div>
				</form>

			</div><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->

	