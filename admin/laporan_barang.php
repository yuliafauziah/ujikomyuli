<?php
include 'header.php';
?>
            <div class="page-content">
        <div class="container-fluid">
            <header class="section-header">
                <div class="tbl">
                    <div class="tbl-row">
                        <div class="tbl-cell">
                            <h2>Inventaris</h2>
                        </div>
                    </div>
                </div>
            </header>

            <section class="card">
                <div class="card-block">
                <div class="form-group">
                        <a href="#"><type="button" data-toggle="modal" data-target="#export" class="btn btn-inline btn-danger">Rekap PDF</a>
                        <a href="#"><type="button" data-toggle="modal" data-target="#excel" class="btn btn-inline btn-success">Rekap Excel</a>
                        </div>
                
                    <div class="table-responsive">
                        <table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Id Inventaris</th>
                                <th>Nama Barang</th>
                                <th>Kondisi</th>
                                <th>Spesifikasi</th>
                                <th>Keterangan</th>
                                <th>Jumlah</th>
                                <th>Nama Jenis</th>
                                <th>Tanggal Register</th>
                                <th>Nama Ruang</th>
                                <th>kode inventaris</th>
                                <th>Nama Petugas</th>
                                <th>Sumber</th>
                              </tr>
                            </thead>
                            <tbody>
                                        <?php
                                        include "../koneksi.php";
                                        $no=1;
                                        $select=mysqli_query($koneksi,"select * from inventaris INNER JOIN jenis ON inventaris.id_jenis=jenis.id_jenis INNER JOIN ruang ON inventaris.id_ruang=ruang.id_ruang INNER JOIN petugas ON inventaris.id_petugas=petugas.id_petugas ORDER BY id_inventaris desc");
                                        while($data=mysqli_fetch_array($select))
                                        {
                                        ?>
                                          <tr>
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $data['id_inventaris']; ?></td>
                                            <td><?php echo $data['nama']; ?></td>
                                            <td><?php echo $data['kondisi']; ?></td>
                                            <td><?php echo $data['spesifikasi']; ?></td>
                                            <td><?php echo $data['keterangan']; ?></td>
                                            <td><?php echo $data['jumlah']; ?></td>
                                            <td><?php echo $data['nama_jenis']; ?></td>
                                            <td><?php echo $data['tgl_register']; ?></td>
                                            <td><?php echo $data['nama_ruang']; ?></td>
                                            <td><?php echo $data['kode_inventaris']; ?></td>
                                            <td><?php echo $data['nama_petugas']; ?></td>
                                            <td><?php echo $data['sumber']; ?></td>

                                            </tr>
                                            <?php
                                        }
                                        ?>                              
                                        </tbody>
                        </table>   
                        </div>
                </div>
            </section>
        </div><!--.container-fluid-->
    </div><!--.page-content-->

<div id="export" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Data Barang</h4>
</div>
<div class="modal-body">
<form action="ctk_pdf.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-danger" value="Cetak">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="ctk_allpdf.php" target="_blank" class="btn btn-info btn-sm" >Cetak Semua</a>
</div>
</div>
</div>
</div>
</div>



<div id="excel" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Data Barang </h4>
</div>
<div class="modal-body">
<form action="ctk_excel.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-success" value="Cetak">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="ctk_all_excel.php" target="_blank" class="btn btn-info btn-sm" >Cetak Semua</a>
</div>
</div>
</div>
</div>
</div>
</div>