<?php
include "header.php";
?>
	
	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h2>Inventaris</h2>
						</div>
					</div>
				</div>
			</header>

			<section class="card">
				<div class="card-block">
				<div class="form-group">
						<a href="#"><type="button" class="btn" data-toggle="modal" data-target="#export">Tambah Data</a>
						</div>
					<div class="table-responsive">
						<table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
							<thead>
							  <tr>
								<th>No</th>
								<th>Id Inventaris</th>
								<th>Nama Barang</th>
								<th>Kondisi</th>
								<th>Spesifikasi</th>
								<th>Keterangan</th>
								<th>Jumlah</th>
								<th>Nama Jenis</th>
								<th>Tanggal Register</th>
								<th>Nama Ruang</th>
								<th>kode inventaris</th>
								<th>Nama Petugas</th>
								<th>Sumber</th>
								<th>Aksi</th>
							  </tr>
							</thead>
							<tbody>
	                                    <?php
	                                    include "../koneksi.php";
	                                    $no=1;
	                                    $select=mysqli_query($koneksi,"select * from inventaris INNER JOIN jenis ON inventaris.id_jenis=jenis.id_jenis INNER JOIN ruang ON inventaris.id_ruang=ruang.id_ruang INNER JOIN petugas ON inventaris.id_petugas=petugas.id_petugas ORDER BY id_inventaris desc");
	                                    while($data=mysqli_fetch_array($select))
	                                    {
	                                    ?>
	                                      <tr>
	                                        <td><?php echo $no++; ?></td>
	                                        <td><?php echo $data['id_inventaris']; ?></td>
	                                        <td><?php echo $data['nama']; ?></td>
											<td><?php echo $data['kondisi']; ?></td>
											<td><?php echo $data['spesifikasi']; ?></td>
											<td><?php echo $data['keterangan']; ?></td>
	                                        <td><?php echo $data['jumlah']; ?></td>
											<td><?php echo $data['nama_jenis']; ?></td>
											<td><?php echo $data['tgl_register']; ?></td>
	                                        <td><?php echo $data['nama_ruang']; ?></td>
											<td><?php echo $data['kode_inventaris']; ?></td>
											<td><?php echo $data['nama_petugas']; ?></td>
											<td><?php echo $data['sumber']; ?></td>
										
	                                            <td><a class="btn btn-rounded btn-inline btn-success-outline fa fa-edit" href="edit_inventaris.php?id_inventaris=<?php echo $data['id_inventaris']; ?>"></a>
	                                            <a class="btn btn-rounded btn-inline btn-danger-outline fa fa-trash-o" href="hapus_inventaris.php?id_inventaris=<?php echo $data['id_inventaris']; ?>"></a></td>    

	                                        </tr>
	                                        <?php
	                                    }
	                                    ?>								
	                                    </tbody>
						</table>
					</div>
				</div>
			</section>
		</div><!--.container-fluid-->
	</div><!--.page-content-->
	<div id="export" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Rekap Absensi </h4>
</div>
<div class="modal-body">
<form action="simpan_inventaris.php" method="post" target="_blank">
<table>
    <div class="form-group row">
						<label class="col-sm-2 form-control-label">Nama Barang</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="nama" type="text" class="form-control" id="inputPassword" required></p>
						</div>
					</div>
    <div class="form-group row">
						<label class="col-sm-2 form-control-label">Kondisi</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="kondisi" type="text" class="form-control" id="inputPassword" required></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Spesifikasi</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="spesifikasi" type="text" class="form-control" id="inputPassword" required></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Keterangan</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="keterangan" type="text" class="form-control" id="inputPassword" required></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Jumlah</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="jumlah" type="text" class="form-control" id="inputPassword" required></p>
						</div>
					</div>
					
					<div class="form-group row">
					<?php
                              include "../koneksi.php";
                              $result = mysqli_query($koneksi,"select * from jenis order by id_jenis asc ");
                              $jsArray = "var id_jenis = new Array();\n";
                               ?> 
						<label class="col-sm-2 form-control-label">Id Jenis</label>
						<div class="col-sm-10">
							 <select class="form-control m-bot15" name="id_jenis" required onchange="changeValue(this.value)">
                                  <option selected="selected">Pilih Jenis
							<?php 
							while($row = mysqli_fetch_array($result)){
							echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
							$jsArray .= "id_jenis['". $row['id_jenis']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
							}
							?>
                             </option>
                             </select> 
						</div>
					</div>
					
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Tanggal Register</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input name="tgl_register" type="date" class="form-control" id="inputPassword" value="<?php $tgl = Date('Y-m-d'); echo $tgl; ?>" required></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Id Ruang</label>
						 <?php
                             include "../koneksi.php";
                             $result = mysqli_query($koneksi,"select * from ruang order by id_ruang asc ");
                             $jsArray = "var id_ruang = new Array();\n";
                             ?> 
						<div class="col-sm-10">
							<select class="form-control m-bot15" name="id_ruang" required onchange="changeValue(this.value">
							 <option selected="selected">Pilih Id Ruang
							<?php 
							while($row = mysqli_fetch_array($result)){
							echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
							$jsArray .= "id_ruang['". $row['id_ruang']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
							}
							?>                       
                            </option>
                            </select>					 
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Kode Inventaris</label>
						<div class="col-sm-10">
							<?php
							$koneksi=mysqli_connect("localhost","root","","ujikom");
							
							$cari_kd=mysqli_query($koneksi,"select max(kode_inventaris)as kode from inventaris");
							$tm_cari=mysqli_fetch_array($cari_kd);
							$kode=substr($tm_cari['kode'],1,4);
							$tambah=$kode+1;
							if($tambah<10){
								$kode_inventaris="V000".$tambah;
							}else{
								$kode_inventaris="V00".$tambah;
							}
							?>
							<p class="form-control-static"><input type="text" id="kode_inventaris" name="kode_inventaris" class="form-control" id="inputPassword" value="<?php echo $kode_inventaris; ?>" required="harus diisi" readonly></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Id Petugas</label>
						 <?php
                             include "../koneksi.php";
                             $result = mysqli_query($koneksi,"select * from petugas order by id_petugas asc ");
                             $jsArray = "var id_petugas = new Array();\n";
                             ?> 
						<div class="col-sm-10">
							<select class="form-control m-bot15" name="id_petugas" required onchange="changeValue(this.value">
                            
							<?php 
							while($row = mysqli_fetch_array($result)){
							echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
							$jsArray .= "id_petugas['". $row['id_petugas']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
							}
							?>
                            </select>
						
					</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Sumber</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="sumber" required type="text" class="form-control" id="inputPassword"></p>
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-inline btn-primary">Simpan</button>
						<a href="inventaris.php" type="button" class="btn btn-inline btn-secondary">Cancel</a>
	
</table>
</form>
</div>
</div>
</div>
</div>
        <script>
            var resizefunc = [];
        </script>