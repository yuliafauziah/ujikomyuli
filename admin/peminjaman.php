<?php
include "header.php";
?>
	
	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h2>Inventaris</h2>
						</div>
					</div>
				</div>
			</header>

			<section class="card">
				<div class="card-block">
				<div class="form-group">
						<a href="#"><type="button" class="btn" data-toggle="modal" data-target="#export">Tambah Data</a>
						</div>
					<div class="table-responsive">
						<table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
							<thead>
							  <tr>
								<th>No</th>
								<th>Nama Barang</th>
                                <th>Jumlah Pinjam</th>
                                <th>Tanggal Pinjam</th>
                                <th>Status</th>
                                <th>Peminjam</th>
                                <th>Action</th>
							  </tr>
							</thead>
							<tbody>
	                                    <?php
	                                    include "../koneksi.php";
	                                    $no=1;
	                                    $select = mysqli_query($koneksi,"select inventaris.nama,peminjaman.tanggal_pinjam,peminjaman.tanggal_kembali, 
										peminjaman.status_peminjaman, peminjaman.id_pegawai, pegawai.nama_pegawai,detail_pinjam.id_detail_pinjam, 
										detail_pinjam.id_inventaris, detail_pinjam.jumlah_pinjam, detail_pinjam.id_peminjaman from detail_pinjam inner join 
										inventaris on inventaris.id_inventaris=detail_pinjam.id_inventaris inner join peminjaman on peminjaman.id_peminjaman=detail_pinjam.id_peminjaman
										inner join pegawai on pegawai.id_pegawai=peminjaman.id_pegawai  where peminjaman.status_peminjaman='pinjam' order by detail_pinjam.id_detail_pinjam desc ");
	                                    while($data=mysqli_fetch_array($select))
	                                    {
	                                    ?>
	                                      <tr>
	                                        <td><?php echo $no++; ?></td>
	                                        <td><?php echo $data['nama']; ?></td>
	                                        <td><?php echo $data['jumlah_pinjam']; ?></td>
											<td><?php echo $data['tanggal_pinjam']; ?></td>
											<td><?php echo $data['status_peminjaman']; ?></td>
											<td><?php echo $data['nama_pegawai']; ?></td>
										
											<td> <a href='proses_kembali.php?id=<?=$data['id_peminjaman'];;?>& idd=<?=$data['id_inventaris'];;?>&jumlah=<?=$data['jumlah_pinjam'];?>' type="button" class="btn btn-inverse"><i class="fa fa-donate"></i> Kembalikan</button></td>

	                                        </tr>
	                                        <?php
	                                    }
	                                    ?>								
	                                    </tbody>
						</table>
					</div>
				</div>
			</section>
		</div><!--.container-fluid-->
	</div><!--.page-content-->
