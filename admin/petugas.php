<?php
include "header.php";
?>
	
	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h2>Inventaris</h2>
						</div>
					</div>
				</div>
			</header>
			<section class="card">
				<div class="card-block">
				<div class="form-group">
						<a href="tambah_petugas.php"><type="button" class="btn">Tambah Data</a>
						</div>
					<div class="table-responsive">
						<table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
							<thead>
							  <tr>
								<th>No</th>
								<th>Id Petugas</th>
								<th>Username</th>
								<th>Password</th>
								<th>Nama Petugas</th>
								<th>Email</th>
								<th>Nama Level</th>
								<th>Aksi</th>
							  </tr>
							</thead>
							<tbody>
	                                    <?php
	                                    include "../koneksi.php";
	                                    $no=1;
	                                    $select=mysqli_query($koneksi,"select * from petugas INNER JOIN level ON petugas.id_level=level.id_level order by id_petugas desc");
	                                    while($data=mysqli_fetch_array($select))
	                                    {
	                                    ?>
	                                      <tr>
	                                        <td><?php echo $no++; ?></td>
	                                        <td><?php echo $data['id_petugas']; ?></td>
	                                        <td><?php echo $data['username']; ?></td>
											<td><?php echo $data['password']; ?></td>
											<td><?php echo $data['nama_petugas']; ?></td>
	                                        <td><?php echo $data['email']; ?></td>
											<td><?php echo $data['nama_level']; ?></td>
										
	                                            <td><a class="btn btn-rounded btn-inline btn-success-outline fa fa-edit" href="edit_petugas.php?id_petugas=<?php echo $data['id_petugas']; ?>"></a>
	                                            <a class="btn btn-rounded btn-inline btn-danger-outline fa fa-trash-o" href="hapus_petugas.php?id_petugas=<?php echo $data['id_petugas']; ?>"></a></td>    

	                                        </tr>
	                                        <?php
	                                    }
	                                    ?>								
	                                    </tbody>
						</table>
					</div>
				</div>
			</section>
		</div><!--.container-fluid-->
	</div><!--.page-content-->