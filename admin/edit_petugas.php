<?php
include "../koneksi.php";
$id_petugas=$_GET['id_petugas'];

$select=mysqli_query($koneksi,"select * from petugas where id_petugas='$id_petugas'");
$data=mysqli_fetch_array($select);
?>

<?php
include "header.php";
?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Edit Petugas</h3>
						</div>
					</div>
				</div>
			</header>

			<div class="box-typical box-typical-padding">
				

				<h5 class="m-t-lg with-border">Horizontal Inputs</h5>

				<form action="update_petugas.php" method="post">
				<input type="hidden" name="id_petugas" value="<?php echo $data['id_petugas'];?>">
				<input type="hidden" name="id_level" value="<?php echo $data['id_level'];?>">
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Username</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="username" required type="text" class="form-control" id="inputPassword" value="<?php echo $data['username'];?>"></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Password</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="password" required type="text" class="form-control" id="inputPassword" value="<?php echo $data['password'];?>"></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Nama Petugas</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="nama_petugas" required type="text" class="form-control" id="inputPassword" value="<?php echo $data['nama_petugas'];?>"></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Email</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input autocomplete="off" name="email" required type="text" class="form-control" id="inputPassword" value="<?php echo $data['email'];?>"></p>
						</div>
					</div>
					<div class="form-group row">
						<label for="exampleSelect" class="col-sm-2 form-control-label">Id Level</label>
						<div class="col-sm-10">
							<input id="exampleSelect" class="form-control" name="id_level" required disabled="" value="<?php echo $data['id_level'];?>">	
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-inline btn-primary">Simpan</button>
						<a href="inventaris.php" type="button" class="btn btn-inline btn-secondary">Cancel</a>
					</div>
				</form>

			</div><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->

	